import sys

from collections import defaultdict as ddict

freq = dict()

for i, arg in enumerate(sys.argv[1:]):
    for ch in open(arg).read():
        if ch not in freq:
            freq[ch] = [0,0,0]
        else:
            freq[ch][i] += 1

for ch in freq:
    print(f'{ch} {freq[ch][0]:7} {freq[ch][1]:7} {freq[ch][2]:7}')
